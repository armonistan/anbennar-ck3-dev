k_dasmazar = {
	930.9.12 = {
		holder = 130 #Dasma szal-Maqet
		government = feudal_government
	}
	978.12.28 = {
		holder = 131 #Idras szal-Maqet
		government = feudal_government
	}
	1014.7.3 = {
		holder = 132 #Maqet II szal-Maqet
		government = feudal_government
	}
}

c_ergalagnu = {
	1000.1.1 = { change_development_level = 9 }
}

c_betharran = {
	1000.1.1 = { change_development_level = 5 }
}

c_tumhar = {
	1000.1.1 = { change_development_level = 5 }
}

c_abadelu = {
	1000.1.1 = { change_development_level = 5 }
}

c_agshzan = {
	1000.1.1 = { change_development_level = 9 }
}

c_istanuz = {
	1000.1.1 = { change_development_level = 12 }
}

c_maqetsad = {
	1000.1.1 = { change_development_level = 10 }
}

c_azka_szel_udam = {
	1000.1.1 = { change_development_level = 18 }
	1014.7.3 = {
		holder = 132 #Maqet II szal-Maqet
	}
}

c_kerusah = {
	1000.1.1 = { change_development_level = 13 }
}

c_dasmatus = {
	1000.1.1 = { change_development_level = 10 }
}