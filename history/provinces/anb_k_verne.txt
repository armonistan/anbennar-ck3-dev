#k_verne
##d_verne
###c_verne
292 = {		#Verne

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
288 = {		#Heartspier

    # Misc
	holding = city_holding

    # History
}
1806 = {

    # Misc
	holding = none

    # History

}
1807 = {

    # Misc
	holding = castle_holding

    # History

}

###c_huntersfield
293 = {		#Huntersfield

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
1808 = {

    # Misc
    holding = city_holding

    # History

}
1809 = {

    # Misc
    holding = none

    # History

}

###c_clawmarches
289 = {		#Clawmarches

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
1810 = {

    # Misc
	holding = none

    # History

}

###c_wyvernmark
290 = {		#Wyvernmark

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
1802 = {

    # Misc
    holding = church_holding

    # History

}
1803 = {

    # Misc
    holding = none

    # History

}

###c_arca_lar
1801 = {	#Arca Lar

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
291 = {

    # Misc
    holding = none

    # History

}
1800 = {

    # Misc
    holding = city_holding

    # History

}


###c_bronzewing
838 = {		#Bronzewing Hall

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = bronzewing_mines_01
		special_building = bronzewing_mines_01
	}
}
1804 = {

    # Misc
    holding = church_holding

    # History

}
1805 = {

    # Misc
    holding = none

    # History

}

##d_the_tail
###c_the_tail
375 = {		#The Tail

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
1796 = {

    # Misc
    holding = none

    # History

}
1797 = {

    # Misc
    holding = none

    # History

}
1798 = {

    # Misc
    holding = city_holding

    # History

}

###c_stingport
376 = {

    # Misc
    holding = city_holding

    # History

}
1790 = {	#Stingport

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History

}
1791 = {

    # Misc
    holding = none

    # History

}
1792 = {

    # Misc
    holding = none

    # History

}

###c_lokkex_heights
377 = {		#Lokkex Heights

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
1799 = {

    # Misc
    holding = none

    # History

}

##d_eastneck
###c_napesbay
50 = {		#Napesbay

    # Misc
    culture = vernid
    religion = luna_damish
	holding = castle_holding

    # History
}
1818 = {

    # Misc
    holding = church_holding

    # History

}
1819 = {

    # Misc
    holding = city_holding

    # History

}

###c_strongport
45 = {		#Strongport

    # Misc
    culture = vernid
    religion = luna_damish
	holding = castle_holding

    # History
}
1820 = {

    # Misc
    holding = none

    # History

}

###c_plainsby
25 = {

    # Misc
    culture = vernid
    religion = luna_damish
	holding = castle_holding

    # History
}
1825 = {

    # Misc
    holding = city_holding

    # History

}
1826 = {

    # Misc
    holding = none

    # History

}

##d_menibor_loop
###c_menibor
569 = {		#Menibor

    # Misc
    culture = vernman
    religion = luna_damish
	holding = castle_holding

    # History
}
1823 = {

    # Misc
    holding = church_holding

    # History

}

1824 = {

    # Misc
    holding = city_holding

    # History

}

###c_oldtower
331 = {

    # Misc
    culture = vernid
    religion = luna_damish
	holding = castle_holding

    # History
}
1822 = {

    # Misc
    holding = none

    # History

}

###c_idhaine
285 = {		#Idhaine

    # Misc
    culture = vernman
    religion = luna_damish
	holding = castle_holding

    # History
}
1829 = {

    # Misc
    holding = none

    # History

}
1830 = {

    # Misc
    holding = city_holding

    # History

}

###c_fadlun
1827 = {	#Valiant Keep

    # Misc
    culture = vernman
    religion = luna_damish
	holding = castle_holding

    # History

}

1828 = {

    # Misc
	holding = none

    # History

}


1831 = {

    # Misc
	holding = church_holding

    # History

}

###c_thanallen
284 = {		#Thanallen

    # Misc
    culture = vernman
    religion = luna_damish
	holding = castle_holding

    # History
}
1832 = {

    # Misc
    holding = city_holding

    # History

}

1833 = {

    # Misc
    holding = none

    # History

}

1834 = {

    # Misc
    holding = church_holding

    # History

}

##d_armanhal
###c_stoutharbor
283 = {

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
1821 = {

    # Misc
    holding = none

    # History

}

###c_armanhal
1816 = {	#Armanhal

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History

}
1813 = {

    # Misc
    holding = none

    # History

}
1815 = {

    # Misc
    holding = castle_holding

    # History

}
364 = {

    # Misc
    holding = city_holding

    # History

}

###c_heroes_rest
286 = {		#Heroes Rest

    # Misc
    culture = vernid
    religion = vernid_adean
	holding = castle_holding

    # History
}
1811 = {

    # Misc
	holding = city_holding

    # History

}

###c_watchmans_point
51 = {		#Watchmans Point

    # Misc
    culture = vernid
    religion = luna_damish
	holding = castle_holding

    # History
}
1817 = {

    # Misc
    holding = none

    # History

}

##d_galeinn
###c_galeinn
295 = {		#Galeinn

    # Misc
    culture = vernman
    religion = luna_damish
	holding = castle_holding

    # History
}
1837 = {

    # Misc
    holding = castle_holding

    # History

}
1838 = {

    # Misc
    holding = none

    # History

}

###c_bellacaire
329 = {		#Bluehart

    # Misc
    culture = old_damerian
    religion = luna_damish
	holding = castle_holding

    # History

}
587 = {		#Bellacaire

    # Misc
    holding = city_holding

    # History

}

###c_whistlepoint
906 = {		#Whistlepoint

    # Misc
    culture = vernid
    religion = luna_damish
	holding = castle_holding

    # History
}
1839 = {

    # Misc
    holding = city_holding

    # History

}

1840 = {

    # Misc
	holding = none

    # History

}

###c_walterton
1835 = {		#Walterton

    # Misc
    culture = vernid
    religion = luna_damish
	holding = castle_holding

    # History
}
287 = {

    # Misc
	holding = city_holding

    # History

}
1836 = {

    # Misc
	holding = none

    # History

}
