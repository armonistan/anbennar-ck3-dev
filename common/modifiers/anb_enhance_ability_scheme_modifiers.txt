﻿enhance_ability_working_harder = {
    icon = treatment_positive
	scheme_phase_duration = minor_scheme_phase_duration_bonus_value
	scheme_success_chance = 10
}
enhance_ability_working_target_pushes_working_harder = {
    icon = treatment_positive
	scheme_phase_duration = medium_scheme_phase_duration_bonus_value
	scheme_success_chance = 10
}
