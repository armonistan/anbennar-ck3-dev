﻿# Anbennar: commenting vanilla stuff

###########
## Generic Holy Site Buildings
###########

# holy_site_cathedral_01 = { }

# holy_site_mosque_01 = { }

# holy_site_fire_temple_01 = { }

# holy_site_pagan_grand_temple_01 = { }

# holy_site_indian_grand_temple_01 = { }

holy_site_other_grand_temple_01 = {
	asset = {
		type = pdxmesh
		name = "building_special_cathedral_generic_mesh"
	}

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_cathedral_zoroastric.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		custom_description = {
			text = holy_site_building_trigger
			barony = {
				is_holy_site_of = scope:holder.faith
			}
		}
		scope:holder = {
			culture = {
				has_innovation = innovation_crop_rotation
			}
		}
	}
	
	is_enabled = {
		custom_description = {
			text = holy_site_building_trigger
			barony = {
				is_holy_site_of = scope:holder.faith
			}
		}
	}
	
	cost_gold = 1000

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.2
		development_growth_factor = 0.2
		development_growth = 0.15
	}
	
	province_modifier = {
		monthly_income = 2
	}
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_religious
}

# NOTE: the custom loc key GetSpecialBuildingNameFromProvince in 00_building_custom_localization.txt pulls from this database. If a new special building is added here, please update the custom loc key!

##########
# Mahabodhi Temple
##########

# holy_site_mahabodhi_temple_01 = { }

##########
# Imam Ali Mosque
##########

# holy_site_imam_ali_mosque_01 = { }

##########
# Great Mosque of Mecca
##########

# holy_site_great_mosque_of_mecca_01 = { }

##########
# Great Mosque of Cordoba
##########

# holy_site_great_mosque_of_cordoba_01 = { }

##########
# Great Mosque of Djenne
##########

# holy_site_great_mosque_of_djenne_01 = { }

##########
# Great Mosque of Samarra
##########

# holy_site_great_mosque_of_samarra_01 = { }

##########
# Cologne Cathedral
##########

# holy_site_cologne_cathedral_01 = { }

##########
# Canterbury Cathedral
##########

# holy_site_canterbury_cathedral_01 = { }

# holy_site_canterbury_cathedral_02 = { }

# holy_site_canterbury_cathedral_03 = { }

##########
# Prophetic Mosque
##########

# holy_site_prophetic_mosque_01 = { }

##########
# Doge's Palace
##########

# doges_palace_01 = { }

##########
# Walls of Genoa
##########

# walls_of_genoa_01 = { }

##########
# Visby Ringmur
##########

# visby_ringmur_01 = { }

##########
# Walls of Benin
##########

# walls_of_benin_01 = { }

##########
# Theodosian Walls
##########

# theodosian_walls_01 = { }

##########
# Aurelian Walls
##########

# aurelian_walls_01 = { }

##########
# The Colosseum
##########

# the_colosseum_01 = { }

##########
# Iron Pillar of Delhi
##########

# iron_pillar_of_delhi_01 = { }

##########
# Iron Pillar of Dhar
##########

# iron_pillar_of_dhar_01 = { }

##########
# The Pyramids
##########

# the_pyramids_01 = { }

##########
# Stonehenge
##########

# stonehenge_01 = { }

##########
# Offa's Dyke
##########

# offas_dyke_01 = { }

##########
# Hadrian's Wall
##########

# hadrians_wall_01 = { }

##########
# Petra
##########

# petra_01 = { }

##########
# Buddhas of Bamian
##########

# buddhas_of_bamian_01 = { }

##########
# Alhambra
##########

# alhambra_01 = { }

# alhambra_02 = { }

##########
# Citadel of Aleppo
##########

# citadel_of_aleppo_01 = { }

# citadel_of_aleppo_02 = { }

##########
# House of Wisdom
##########

# house_of_wisdom_01 = { }

##########
# The Tower of London
##########

# the_tower_of_london_01 = { }

##########
# London Bridge
##########

# london_bridge_01 = { }

##########
# Notre Dame
##########

# notre_dame_01 = { }

##########
# Brihadeeswarar Temple
##########

# brihadeeswarar_temple_01 = { }

##########
# Shwedagon Pagoda
##########

# shwedagon_pagoda_01 = { }

##########
# Ananda Temple
##########

# ananda_temple_01 = { }

##########
# The Friday Mosque
##########

# the_friday_mosque_01 = { }

##########
# Khajuraho
##########

# khajuraho_01 = { }

##########
# Palace of Aachen
##########

# palace_of_aachen_01 = { }

##########
# Hagia Sophia
##########

# hagia_sophia_01 = { }

# hagia_sophia_02 = { }

##########
# Dome of the Rock / Temple in Jerusalem
##########

# dome_of_the_rock_01 = { }

# temple_in_jerusalem_01 = { }

##########
# Hall of Heroes
##########

# hall_of_heroes_01 = { }

# hall_of_heroes_02 = { }

# hall_of_heroes_03 = { }

# hall_of_heroes_04 = { }

# hall_of_heroes_05 = { }

##########
# Universities
##########

generic_university = {
	construction_time = very_slow_construction_time
	effect_desc = {
		desc = unlocks_building_desc
		triggered_desc = {
			trigger = { has_dlc_feature = tours_and_tournaments }
			desc = university_toto_effect_desc
		}
		triggered_desc = {
			trigger = { has_dlc_feature = royal_court }
			desc = university_roco_effect_desc
		}
		desc = university_effect_desc
	}

	type_icon = "icon_building_university.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
		building_university_requirement = yes
	}
	
	can_construct = {
		scope:holder = {
			highest_held_title_tier >= tier_duchy
			prestige_level >= 4
		}
		county = {
			development_level >= 30
		}
	}
	
	show_disabled = yes
	
	on_complete = {
		county.holder = {
			save_scope_as = founder

			found_university_decision_event_effect = yes

			every_player = {
				limit = {
					NOT = { this = scope:founder }
					is_within_diplo_range = { CHARACTER = scope:founder }
				}
				trigger_event = major_decisions.2002
			}
		}
	}
	
	cost_gold = 1000

	character_modifier = {
		learning_per_prestige_level = 1
		monthly_lifestyle_xp_gain_mult = 0.1
		cultural_head_fascination_mult = 0.05
		monthly_dynasty_prestige = 0.25
	}
	
	county_modifier = {
		development_growth_factor = 0.1
		development_growth = 0.1
	}
	
	ai_value = {
		base = 8
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_learning
}

# al_azhar_university = { }

# sankore_university = { }

# siena_university = { }

# nalanda_university = { }

########
#Sicilian_parliament
########

# special_sicilian_parliament_01 = { }

########
#Greenhouse
########

special_greenhouse_01 = {
	construction_time = very_slow_construction_time
	
	is_enabled = {
		has_variable = variable_greenhouse
	}
	
	province_modifier = {
		monthly_income = 1
	}

	county_modifier = {
		development_growth_factor = 0.25
		county_opinion_add = 25
	}
	character_modifier = {
		health = 0.25
		monthly_prestige = 1
	}

	type_icon = "icon_building_generic_house.dds"
	
	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_learning
}

##########
# Hotin Fortress (AKA: Khotyn Fortress)
##########

# hotin_fort_01 = { }

# hotin_fort_02 = { }

##########
# Golden Gate of Kiev
##########

# golden_gate_of_kiev_01 = { }

##########
# Heddal Stave Church
##########

# heddal_stave_church_01 = { }

##########
# Temple of Uppsala
##########

# temple_of_uppsala_01 = { }

# temple_of_uppsala_02 = { }

##########
# Lund Cathedral
##########

# lund_cathedral_01 = { }

##########
# Danevirke
##########

# danevirke_01 = { }

##########
# Roman Walls of Lugo 
##########

# roman_wall_of_lugo_01 = { }

##########
# Rock of Gibraltar  
##########

# rock_of_gibraltar_01 = { }

##########
# Aljaferia Palace  
##########

# aljaferia_palace_01 = { }

##########
# Tower of Hercules  
##########

# tower_of_hercules_01 = { }

##########
# City Walls of Toledo 
##########

# city_wall_of_toledo_01 = { }

##########
# Alcazar de Segovia  
##########

# alcazar_of_segovia_01 = { }

##########
# The Santiago de Compostela Archcathedral Basilica 
##########

# holy_site_basilica_santiago_01 = { }

##########
# Sancutary of Imam Reza
##########

# imam_reza_shrine_01 = { }

##########
# Dome of Soltaniyeh
##########

# soltaniyeh_01 = { }

##########
# Ruins of the Palace of Ctesiphon
##########

# palace_of_ctesiphon_01 = { }

# palace_of_ctesiphon_02 = { }

##########
# Falak-ol-Aflak
##########

# falak_ol_aflak_citadel_01 = { }

##########
# Minarets of Jam
##########

# minarets_and_remains_of_jam_01 = { }

##########
# Great Wall of Gorgan
##########

# great_wall_of_gorgan_01 = { }

##########
# Lake Maharloo
##########

# maharloo_lake_01 = { }

##########
# Rainbow Mountains
##########

# rainbow_mountains_01 = { }

##########
# Mount Damavand
##########

# mount_damavand_01 = { }

##########
# Tomb of Batsheba
##########

# tomb_of_cyrus_01 = { }

##########
# Ark of Bukhara
##########

# ark_of_bukhara_01 = { }

##########
# Shah-i-Zinda
##########

# shah_i_zinda_01 = { }

# shah_i_zinda_02 = { }

##########
# Alamut Castle
##########

# alamut_castle_01 = { }

# alamut_castle_02 = { }

assassin_castle_01 = {

	asset = {
		type = pdxmesh
		name = "fp3_building_special_alamut_castle_01_a_mesh"
	}
	
	construction_time = very_slow_construction_time
	
	type_icon = "icon_structure_hotin_fortress.dds"

	cost_gold = 600
	cost_piety = 400

	character_modifier = {
		max_hostile_schemes_add = 1
		monthly_piety = 0.5
		learning = 1
		monthly_county_control_growth_factor = 0.2
	}

	province_modifier = {
		travel_danger = -50
		hostile_raid_time = 3
		stationed_maa_toughness_mult = normal_maa_toughness_tier_4
		fort_level = good_building_fort_level_tier_4
	}

	county_modifier = {
		defender_holding_advantage = 4
		monthly_county_control_growth_add = 1
	}
	
	ai_value = {
		base = 100
		culture_likely_to_fortify_modifier = yes
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	}
	
	type = special

	flag = travel_point_of_interest_martial # fortress
}

##################
# Event Tower of Silence (typically a duchy building, this is a special for FP3 events)
##################
event_tower_of_silence_01 = {

	construction_time = very_slow_construction_time

	type_icon = "icon_building_tower_of_silence.dds"

	can_construct_potential = {
		always = no
	}

	cost_gold = expensive_building_tier_3_cost

	county_modifier = {
		county_opinion_add = 5
		development_growth_factor = 0.10
	}

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for duchy buildings
			factor = 0
			free_building_slots > 0
		}
	}

	type = special

	flag = travel_point_of_interest_religious # funeral site
}

############ EP3 ###############

##########
# Church of the Theotokos
##########
# church_of_the_theotokos = { }

# parthenon = { }

##########
# Hagios Demetrios
##########
# hagios_demetrios = { }

##########
# Kassiopi Castle
##########
# kassiopi_castle = { }

##########
# Church of St. Lazarus
##########
# saint_lazarus = { }

##########
# Sumela Monastery
##########
# sumela_monastery_01 = { }

# sumela_monastery_02 = { }

##########
# Patras Castle
##########
# patras_castle = { }

##########
# Maiden's Tower
##########
# maiden_tower = { }

##########
# Despots' Palace at Mystras
##########
# despot_palace = { }

##########
# Meteora
##########
# meteora_01 = { }

# meteora_02 = { }

##########
# Mt. Athos
##########
# mount_athos_01 = { }

# mount_athos_02 = { }

# mount_athos_03 = { }

##########
# Sant'Apollinare Nuovo
##########
# apollinare_nuovo = { }

##########
# Saint Chatherine's Monastery
##########
# saint_catherine = { }

##########
# Jvari Monastery
##########
# jvari_monastery = { }

##########
# Etchmiadzin Cathedral
##########
# etchmiadzin_cathedral = { }

##########
# Cattolica di Stilo
##########
# cattolica_stilo = { }

##########
# Hosios Loukas
##########
# hosios_loukas = { }

##########
# Church of Saint Sophia, Ohrid
##########
# sofia_ohrid = { }

##########
# Fairy Chimneys
##########
# fairy_chimneys = { }

##########
# Cilician Gates
##########
# cilician_gates = { }
