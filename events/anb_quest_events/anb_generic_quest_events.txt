﻿# namespace = anb_generic_quest_events

# @very_easy_quest_duel = 5
# @easy_quest_duel = 7
# @medium_quest_duel = 10
# @hard_quest_duel = 15

# #Adventurer insults vassal
# anb_generic_quest_events.0001 = {
	# title = anb_generic_quest_events.0001.title
	# desc = anb_generic_quest_events.0001.description
	# theme = court
	
	# right_portrait = {
		# character = scope:inspiration_owner
	# }
	
	# left_portrait = {
		# character = scope:insulted_vassal
	# }
	
	# trigger = {
		# # You don't own the quest location
		# scope:quest_location = {
			# NOT = { province_owner = scope:inspiration_sponsor }
		# }
	# }
	
	# immediate = {
		# scope:quest_location = {
			# province_owner = {
				# save_scope_as = insulted_vassal
			# }
		# }
	# }
	
	# weight_multiplier = {
		# base = 1
		# modifier = {
			# add = 0.5
			# NOT = {
				# scope:inspiration_owner = {
					# diplomacy > 10
				# }
			# }
		# }
		# modifier = {
			# add = 0.5
			# NOT = {
				# scope:inspiration_owner = {
					# diplomacy > 5
				# }
			# }
		# }
		# modifier = {
			# add = 0.5
			# scope:inspiration_owner = {
				# has_trait = peasant_leader
			# }
		# }
	# }

	# #TODO - AI Chances
	# # Character is there to do a job, allow them do as they please
	# option = {
		# name = anb_generic_quest_events.0001.a
		# scope:insulted_vassal = {
			# add_opinion = {
				# modifier = respect_opinion
				# opinion = -15
				# target = scope:inspiration_sponsor
			# }
			# add_opinion = {
				# modifier = respect_opinion
				# opinion = -15
				# target = scope:inspiration_owner
			# }
		# }

		# random_list = {
			# 30 = { 
				# scope:inspiration = {
					# change_inspiration_progress = 1
				# }
			# }
			# 2 = {
				# custom_tooltip = province_owner_will_retaliate
				# hidden_effect = {
					# trigger_event = anb_generic_quest_events.0002
				# }
			# }
			# 68 = { }
		# }
	# }
	
	# # Adventurer must apologize
	# option = {
		# name = anb_generic_quest_events.0001.b

		# scope:inspiration = {
			# random = { 
				# chance = 30
				# change_inspiration_progress = -1
			# }
		# }
		
		# scope:inspiration_owner = {
			# add_opinion = {
				# modifier = respect_opinion
				# opinion = -5
				# target = scope:inspiration_owner
			# }
		# }
	# }
	
	# # Apologize yourself
	# option = {
		# name = anb_generic_quest_events.0001.b
		# add_prestige_no_experience = -250
		
		# scope:insulted_vassal = {
			# add_opinion = {
				# modifier = respect_opinion
				# opinion = -5
				# target = scope:inspiration_owner
			# }
		# }
	# }
	
	# # Ignore it
	# option = {
		# name = anb_generic_quest_events.0001.c
		
		# scope:insulted_vassal = {
			# add_opinion = {
				# modifier = respect_opinion
				# opinion = -10
				# target = scope:inspiration_sponsor
			# }
			# add_opinion = {
				# modifier = respect_opinion
				# opinion = -15
				# target = scope:inspiration_owner
			# }
		# }
		
		# random_list = {
			# 2 = {
				# custom_tooltip = province_owner_will_retaliate
				# hidden_effect = {
					# trigger_event = anb_generic_quest_events.0002
				# }
			# }
			# 68 = { }
		# }
	# }
# }

# #Province owner wants to expel adventurer - TODO should add an event for the vassal so players know what's going on
# anb_generic_quest_events.0002 = {
	# title = anb_generic_quest_events.0002.title
	# desc = anb_generic_quest_events.0002.description
	# theme = court
	
	# right_portrait = {
		# character = scope:inspiration_owner
	# }
	
	# left_portrait = {
		# character = scope:landowner
	# }
	
	# trigger = {
		# # You don't own the quest location
		# scope:quest_location = {
			# NOT = { province_owner = scope:inspiration_sponsor }
			# province_owner = {
				# opinion = { target = scope:inspiration_owner value < 0 }
				# NOT = { has_character_modifier = accepted_adventurer }
			# }
		# }
	# }

	# immediate = {
	# }
	
	# weight_multiplier = {
		# base = 1

		# modifier = {
			# add = 2
			# scope:inspiration_owner = {
			# var:quest_location = {
					# province_owner = {
						# opinion = { target = scope:inspiration_owner value < -20 }
					# }
				# }
			# }
		# }
		# modifier = {
			# add = 2
			# scope:inspiration_owner = {
			# var:quest_location = {
					# province_owner = {
						# opinion = { target = scope:inspiration_owner value < -40 }
					# }
				# }
			# }
		# }
	# }
	
	# # Damn.
	# option = {
		# name = anb_generic_quest_events.0002.b
		
		# destroy_inspiration = scope:inspiration
	# }

	# # Perhaps we could negotiate?
	# option = {
		# name = anb_generic_quest_events.0002.b
		
		# duel = {
			# skill = diplomacy
			# target = scope:landowner
			# 30 = {
				# desc = anb_generic_quest_events.success
				# compare_modifier = {
					# value = scope:duel_value
					# multiplier = 0.5
				# }
				
				# hidden_effect = {
					# scope:landowner = {
						# add_character_modifier = {
							# modifier = accepted_adventurer
							# days = 1825
						# }
					# }
				# }
				
				# send_interface_toast = {
					# title = hold_court.3000.t
					# left_icon = scope:landowner
					# custom_tooltip = anb_generic_quest_events.success
				# }
			# }
			# 20 = {
				# desc = anb_generic_quest_events.failure
				
				# destroy_inspiration = scope:inspiration

				# send_interface_toast = {
					# title = hold_court.3000.t
					# left_icon = scope:landowner
					# custom_tooltip = anb_generic_quest_events.failure
				# }
			# }
		# }
	# }
	
	# # I could pay you?
	# option = {
		# name = anb_generic_quest_events.0002.c
		
		# hidden_effect = {
			# scope:landowner = {
				# add_character_modifier = {
					# modifier = accepted_adventurer
					# days = 1825
				# }
			# }
		# }
		
		# pay_short_term_income = {
			# target = scope:landowner
			# months = 2
		# }
	# }
# }

# # Influx of adventurers brings business
# anb_generic_quest_events.0003 = {
	# title = anb_generic_quest_events.0003.title
	# desc = anb_generic_quest_events.0003.description
	# theme = court
	
	# left_portrait = {
		# character = scope:inspiration_owner
	# }
	
	# trigger = {
		# primary_title = {
			# is_de_facto_liege_or_above_target = scope:quest_location.county
		# }
	# }

	# # Great!
	# option = {
		# name = anb_generic_quest_events.0003.a
		
		# scope:quest_location = {
			# add_county_modifier = {
				# modifier = influx_of_adventurer_business
				# days = 1825
			# }
		# }
	# }
	
	# # This will drive up prices!
	# option = {
		# name = anb_generic_quest_events.0003.b
		# skill = stewardship
		# trigger = {
			# stewardship < 15
		# }
		
		# scope:quest_location = {
			# add_county_modifier = {
				# modifier = influx_of_adventurer_business_high_prices
				# days = 1825
			# }
		# }
	# }
	
	# # This will become the land of adventurers!
	# option = {
		# name = anb_generic_quest_events.0003.c
		# trait = adventurer
		# trigger = {
			# has_trait = adventurer
		# }
		
		# add_prestige = 150
		
		# scope:quest_location = {
			# add_county_modifier = {
				# modifier = influx_of_adventurer_business_adventurer
				# days = 1825
			# }
		# }
	# }
# }

# # Adventurers used as cheap labour
# anb_generic_quest_events.0004 = {
	# title = anb_generic_quest_events.0004.title
	# desc = anb_generic_quest_events.0004.description
	# theme = court
	
	# left_portrait = {
		# character = scope:inspiration_owner
	# }
	
	# trigger = {
		# primary_title = {
			# is_de_facto_liege_or_above_target = scope:quest_location.county
		# }
	# }

	# # Exploit them for manual labour
	# option = {
		# name = anb_generic_quest_events.0004.a
		
		# scope:quest_location = {
			# add_county_modifier = {
				# modifier = influx_of_adventurer_labour
				# days = 1825
			# }
			
			# set_variable = { name = expoiting_adventurer_labour days = 1825 }
		# }

		# hidden_effect = {
			# random = {
				# chance = 5
				# trigger_event = { id = anb_generic_quest_events.0005 days = { 60 120 } }
			# }
		# }
		
		# stress_impact = {
			# compassionate = medium_stress_gain
			# greedy = minor_stress_loss
			# just = minor_stress_gain
			# arbitrary = minor_stress_loss
			# diligent = minor_stress_loss
		# }
	# }
	
	# # Make a deal with them for exclusive mercenary contracts
	# option = {
		# name = anb_generic_quest_events.0004.b
		
		# add_character_modifier = {
			# modifier = adventurer_mercenaries
			# days = 1825
		# }
		
		# stress_impact = {
			# greedy = minor_stress_loss
		# }
	# }
	
	# # My friends! Come fight for me!
	# option = {
		# name = anb_generic_quest_events.0004.c
		# trait = adventurer
		# trigger = {
			# has_trait = adventurer
		# }
		
		# show_as_unavailable = {
			# NOT = { gold > 100 }
		# }

		# add_prestige_no_experience = -50
		# remove_short_term_gold = 100
		# spawn_army = {
			# men_at_arms = {
				# type = adventurers
				# stacks = 4
			# }
			# location = capital_province
			# origin = scope:quest_location
			# inheritable = no
			# uses_supply = no
			# name = adventurer_band # gives the troops a specific name that shows up in interfaces
		# }
		
		# stress_impact = {
			# ambitious = medium_stress_loss
			# shy = medium_stress_gain
			# arrogant = medium_stress_loss
			# brave = medium_stress_loss
		# }
	# }
# }

# # Adventurers damage building
# anb_generic_quest_events.0005 = {
	# title = anb_generic_quest_events.0005.title
	# desc = anb_generic_quest_events.0005.description
	# theme = feast_activity
	
	# left_portrait = {
		# character = scope:inspiration_owner
	# }
	
	# right_portrait = {
		# character = scope:province_owner
	# }
	
	# immediate = {
		# scope:quest_location = {
			# province_owner = {
				# save_scope_as = province_owner
			# }
			# remove_county_modifier = influx_of_adventurer_labour
			# add_county_modifier = {
				# modifier = adventurer_damaged_building
				# days = 365
			# }
			# change_development_level = -1
		# }
	# }
	
	# # Damn
	# option = {
		# name = anb_generic_quest_events.0005.a
	
		# scope:province_owner = {			
			# add_opinion = {
				# modifier = respect_opinion
				# opinion = -15
				# target = scope:inspiration_owner
			# }
		# }
		
		# if = {
			# limit = {
				# has_variable = expoiting_adventurer_labour
			# }

			# add_prestige_no_experience = -250

			# stress_impact = {
				# diligent = minor_stress_gain
				# just = minor_stress_gain
			# }
		# }
	# }
# }

# #Adventurer goes to the tavern to relax
# anb_generic_quest_events.0006 = {
	# title = anb_generic_quest_events.0006.title
	# desc = anb_generic_quest_events.0006.description
	# theme = feast_activity
	
	# left_portrait = {
		# character = scope:inspiration_owner
	# }
	
	# weight_multiplier = {
		# base = 1

		# modifier = {
			# add = 1
			# scope:inspiration_owner = {
				# has_trait = gluttonous
			# }
		# }
		# modifier = {
			# add = 1
			# scope:inspiration_owner = {
				# has_trait = drunkard
			# }
		# }
		# modifier = {
			# factor = 0.5
			# scope:inspiration_owner = {
				# has_trait = diligent
			# }
		# }
	# }
	
	# # They can stay for a while
	# option = {
		# name = anb_generic_quest_events.0006.a
		
		# scope:inspiration_owner = {
			# stress_impact	 = {
				# base = minor_stress_loss
				# drunkard = minor_stress_loss
				# gluttonous = minor_stress_loss
			# }
		# }
		
		# scope:inspiration = {
			# change_inspiration_progress = -1
		# }
		
		# adventurer_remain_in_tavern_effect = yes
	# }
	
	# # Send them back out
	# option = {
		# name = anb_generic_quest_events.0006.b
		
		# scope:inspiration_owner = {
			# stress_impact	 = {
				# drunkard = minor_stress_gain
				# gluttonous = minor_stress_gain
			# }
			
			# add_opinion = {
				# modifier = respect_opinion
				# opinion = -15
				# target = scope:inspiration_sponsor
			# }
		# }
	# }
# }

# # Adventurer remains in tavern
# anb_generic_quest_events.0007 = {
	# title = anb_generic_quest_events.0007.title
	# desc = anb_generic_quest_events.0007.description
	# theme = feast_activity
	
	# left_portrait = {
		# character = scope:inspiration_owner
	# }
	
	# # They can stay for a while
	# option = {
		# name = anb_generic_quest_events.0006.a
		
		# scope:inspiration_owner = {
			# stress_impact	 = {
				# base = minor_stress_loss
				# drunkard = minor_stress_loss
				# gluttonous = minor_stress_loss
			# }
			
			# add_prestige_no_experience = minor_prestige_loss
		# }
		
		# scope:inspiration_sponsor = {
			# add_prestige_no_experience = minor_prestige_loss
		# }
		
		# scope:inspiration = {
			# change_inspiration_progress = -1
		# }
		
		# if = {
			# limit = {
				# NOT = {
					# scope:inspiration_owner = {
						# has_trait = drunkard
					# }
				# }
			# }
			# random = {
				# chance = 20
				# scope:inspiration_owner = {
					# add_trait = drunkard
				# }
			# }
		# }
		
		
		# adventurer_remain_in_tavern_effect = yes
	# }
	
	# # Send them back out
	# option = {
		# name = anb_generic_quest_events.0006.b
		
		# scope:inspiration_owner = {
			# stress_impact	 = {
				# drunkard = minor_stress_gain
				# gluttonous = minor_stress_gain
			# }
		# }
	# }
# }

# # Adventurer finds lead in tavern
# anb_generic_quest_events.0008 = {
	# title = anb_generic_quest_events.0008.title
	# desc = anb_generic_quest_events.0008.description
	# theme = feast_activity
	
	# left_portrait = {
		# character = scope:inspiration_owner
	# }

	# # Fantastic
	# option = {
		# name = anb_generic_quest_events.0008.a
		
		# scope:inspiration_sponsor = {
			# add_prestige = minor_prestige_gain
		# }
		
		# scope:inspiration = {
			# change_inspiration_progress = 1
		# }
	# }
# }

# # Garrison is a nuisance
# anb_generic_quest_events.0009 = {
	# title = anb_generic_quest_events.0009.title
	# desc = anb_generic_quest_events.0009.description
	# theme = war
	
	# left_portrait = {
		# character = scope:inspiration_owner
	# }
	
	# immediate = {
		# scope:quest_location = {
			# add_county_modifier = {
				# modifier = army_quest_presence
				# days = 365
			# }
		# }
	# }
	
	# trigger = {
		# NOT = {
			# scope:quest_location = {
				# has_county_modifier = army_quest_presence
			# }
		# }
	# }
	
	# # They should leave it to the professionals
	# option = {
		# name = anb_generic_quest_events.0009.a
		
		# quest_duel_effect = {
			# TITLE = "anb_goblin_quest.0007.title"
			# SUCCESS = "anb_goblin_quest.duel.success"
			# SUCCESS_EFFECT = "
				# scope:quest_location = {
					# remove_county_modifier = army_quest_presence
				# }
			# "
			# FAILURE = "anb_goblin_quest.duel.failure"
			# FAILURE_EFFECT = "
				# scope:inspiration_sponsor = {
					# add_prestige = minor_prestige_loss
				# }
				
				# random = {
					# chance = 20
					# scope:inspiration = {
						# change_inspiration_progress = -1
					# }
				# }
			# "
			# SKILL = "martial"
			# DIFFICULTY = @medium_quest_duel
		# }
	# }
	
	# # Perhaps we could use their help...
	# option = {
		# name = anb_generic_quest_events.0009.b
		
		# random = {
			# chance = 20
			# scope:inspiration = {
				# change_inspiration_progress = -1
			# }
		# }
	# }
# }